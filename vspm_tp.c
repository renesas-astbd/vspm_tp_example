#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <linux/ioctl.h>
#include <sys/mman.h>

#include "vspm_public.h"
#include "mmngr_user_public.h"

long fbfd;
struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

unsigned long output_phys;
unsigned long output_hard;
unsigned long output_virt;
MMNGR_ID fd;

volatile unsigned char end_flag = 0;

static int open_fb(void);
static int close_fb(void);
static void output_fb(void);
static int allocate_memory(void);
static int release_memory(void);

/* callback function */
static void cb_func(
	unsigned long uwJobId, long wResult, unsigned long uwUserData)
{
	end_flag = 1;
}

/* main function */
int main(int argc, char *argv[])
{
	VSPM_IP_PAR vspm_ip;
	VSPM_VSP_PAR vsp_par;

	T_VSP_IN src_par;
	T_VSP_ALPHA src_alpha_par;
	T_VSP_OUT dst_par;
	T_VSP_CTRL ctrl_par;

	T_VSP_UDS uds_par;

	T_VSP_BRU bru_par;
	T_VSP_BLEND_CONTROL bru_ctrl_par;
	T_VSP_BLEND_VIRTUAL bru_vir_par;

	unsigned long handle;
	unsigned long jobid;

	long ercd;

	if (open_fb()) {
		printf("open fb failed!!\n");
		return -1;
	}

	if (allocate_memory()) {
		printf("allocate memory failed!!\n");
		(void)close_fb();
		return -1;
	}

	/* Initilise VSP manager */
	ercd = VSPM_lib_DriverInitialize(&handle);
	if (ercd) {
		printf("VSPM_lib_DriverInitialize() Failed!! ercd=%d\n", ercd);
		(void)release_memory();
		(void)close_fb();
		return -1;
	}

	{
		src_alpha_par.addr_a	= NULL;
		src_alpha_par.alphan	= VSP_ALPHA_NO;
		src_alpha_par.alpha1	= 0;
		src_alpha_par.alpha2	= 0;
		src_alpha_par.astride	= 0;
		src_alpha_par.aswap		= VSP_SWAP_NO;
		src_alpha_par.asel		= VSP_ALPHA_NUM5;
		src_alpha_par.aext		= VSP_AEXT_EXPAN;
		src_alpha_par.anum0		= 0;
		src_alpha_par.anum1		= 0;
		src_alpha_par.afix		= 0xff;
		src_alpha_par.irop		= VSP_IROP_NOP;
		src_alpha_par.msken		= VSP_MSKEN_ALPHA;
		src_alpha_par.bsel		= 0;
		src_alpha_par.mgcolor	= 0;
		src_alpha_par.mscolor0	= 0;
		src_alpha_par.mscolor1	= 0;

		src_par.addr		= NULL;
		src_par.addr_c0		= NULL;
		src_par.addr_c1		= NULL;
		src_par.stride 		= 1280 * 4;
		src_par.stride_c	= 0;
		src_par.width		= 1280;
		src_par.height		= 768;
		src_par.width_ex	= 0;
		src_par.height_ex	= 0;
		src_par.x_offset	= 0;
		src_par.y_offset	= 0;
		src_par.format		= VSP_IN_ARGB8888;
		src_par.swap		= VSP_SWAP_NO;
		src_par.x_position	= 0;
		src_par.y_position	= 0;
		src_par.pwd			= VSP_LAYER_PARENT;
		src_par.cipm		= VSP_CIPM_0_HOLD;
		src_par.cext		= VSP_CEXT_EXPAN;
		src_par.csc			= VSP_CSC_OFF;
		src_par.iturbt		= VSP_ITURBT_709;
		src_par.clrcng		= VSP_FULL_COLOR;
		src_par.vir			= VSP_VIR;
		src_par.vircolor	= 0x00FF0000;
		src_par.osd_lut		= NULL;
		src_par.alpha_blend	= &src_alpha_par;
		src_par.clrcnv		= NULL;
		src_par.connect		= VSP_UDS_USE;
	}

	{
		dst_par.addr		= (void*)output_hard;
		dst_par.addr_c0		= NULL;
		dst_par.addr_c1		= NULL;
		dst_par.stride		= vinfo.xres * vinfo.bits_per_pixel / 8;
		dst_par.stride_c	= 0;
		dst_par.width		= 1280;
		dst_par.height		= 768;
		dst_par.x_offset	= 0;
		dst_par.y_offset	= 0;
		dst_par.format		= VSP_OUT_PRGB8888;
		dst_par.swap		= VSP_SWAP_L|VSP_SWAP_LL;
		dst_par.pxa			= VSP_PAD_P;
		dst_par.pad			= 0;
		dst_par.x_coffset	= 0;
		dst_par.y_coffset	= 0;
		dst_par.csc			= VSP_CSC_OFF;
		dst_par.iturbt		= VSP_ITURBT_709;
		dst_par.clrcng		= VSP_ITU_COLOR;
		dst_par.cbrm		= VSP_CSC_ROUND_DOWN;
		dst_par.abrm		= VSP_CONVERSION_ROUNDDOWN;
		dst_par.athres		= 0;
		dst_par.clmd		= VSP_CLMD_NO;
		dst_par.dith		= VSP_NO_DITHER;
	}

	{
		{	/* UDS0 */
			memset(&uds_par, 0, sizeof(T_VSP_UDS));
			uds_par.fmd			= VSP_FMD;
			uds_par.filcolor	= 0x0000FF00;	/* green */
			uds_par.x_ratio		= 0x2000;		/* 1/2 */
			uds_par.y_ratio		= 0x1000;
			uds_par.out_cwidth	= 1280;
			uds_par.out_cheight	= 768;
			uds_par.connect		= VSP_BRU_USE;
		}

		{	/* BRU */
			memset(&bru_ctrl_par, 0, sizeof(T_VSP_BLEND_CONTROL));
			bru_ctrl_par.rbc			= VSP_RBC_BLEND;
			bru_ctrl_par.blend_coefx	= VSP_COEFFICIENT_BLENDX5;
			bru_ctrl_par.blend_coefy	= VSP_COEFFICIENT_BLENDY5;
			bru_ctrl_par.acoefx 		= VSP_COEFFICIENT_ALPHAX5;
			bru_ctrl_par.acoefy 		= VSP_COEFFICIENT_ALPHAY5;
			bru_ctrl_par.acoefx_fix		= 0x80;
			bru_ctrl_par.acoefy_fix		= 0x80;

			memset(&bru_vir_par, 0, sizeof(T_VSP_BLEND_VIRTUAL));
			bru_vir_par.width		= 640;
			bru_vir_par.height  	= 480;
			bru_vir_par.x_position 	= 320;
			bru_vir_par.y_position 	= 144;
			bru_vir_par.pwd			= VSP_LAYER_CHILD;
			bru_vir_par.color		= 0x800000FF;	/* blue */

			memset(&bru_par, 0, sizeof(T_VSP_BRU));
			bru_par.lay_order		= (VSP_LAY_VIRTUAL | (VSP_LAY_1 << 4));
			bru_par.blend_virtual 	= &bru_vir_par;
			bru_par.blend_control_a = &bru_ctrl_par;
			bru_par.connect			= 0;		/* connect WPF */
		}

		memset(&ctrl_par, 0, sizeof(T_VSP_CTRL));
		ctrl_par.uds	= &uds_par;
		ctrl_par.bru	= &bru_par;
	}

	vsp_par.rpf_num		= 1;
	vsp_par.use_module	= VSP_UDS_USE | VSP_BRU_USE;
	vsp_par.src1_par	= &src_par;
	vsp_par.src2_par	= NULL;
	vsp_par.src3_par	= NULL;
	vsp_par.src4_par	= NULL;
	vsp_par.dst_par		= &dst_par;
	vsp_par.ctrl_par	= &ctrl_par;

	memset(&vspm_ip, 0, sizeof(VSPM_IP_PAR));
	//assignable & VSPM_IPCTRL_VSPD0_WPF_CH0 failed
	//VSPM_IP_MAX = ?
	//#ifdef USE_VSPD0
	vspm_ip.uhType		= VSPM_TYPE_VSP_VSPD0;//VSPM_TYPE_VSP_AUTO;
	vspm_ip.unionIpParam.ptVsp = &vsp_par;

	end_flag = 0;
	ercd = VSPM_lib_Entry(handle, &jobid, 126, &vspm_ip, 0, cb_func); 
	if (ercd) {
		printf("VSPM_lib_Entry() Failed!! ercd=%d\n", ercd);
		(void)VSPM_lib_DriverQuit(handle);
		(void)release_memory();
		(void)close_fb();
		return -1;
	}

	while(1) {
		if (end_flag)	break;
	}

	/* output frame buffer */
	output_fb();

	/* Finalise VSP manager */
	ercd = VSPM_lib_DriverQuit(handle);
	if (ercd) {
		printf("VSPM_lib_DriverQuit() Failed!! ercd=%d\n", ercd);
	}

	if (release_memory()) {
		printf("release memory failed!!\n");
	}

	if (close_fb()) {
		printf("close fb failed!!\n");
	}

	return 0;
}

static int open_fb(void)
{
	int ercd;

	fbfd = open("/dev/fb0", O_RDWR);
	if (fbfd < 0) {
		printf("open() failed!! ercd=%d\n", fbfd);
		return -1;
	}

	ercd = ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo);
	if (ercd) {
		printf("ioctl(FBIOGET_FSCREENINFO) Failed!! ercd=%d\n", ercd);
		close(fbfd);
		return -1;
	}

	ercd = ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo);
	if (ercd) {
		printf("ioctl(FBIOGET_VSCREENINFO) Failed!! ercd=%d\n", ercd);
		close(fbfd);
		return -1;
	}

	return 0;
}

static int close_fb(void)
{
	int ercd;

	ercd = close(fbfd);
	if (ercd) {
		printf("close() Failed!! ercd=%d\n", ercd);
		return -1;
	}

	return 0;
}

/* output frame buffer */
static void output_fb(void)
{
	char *fbp;

	int ercd;
	unsigned long screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

	fbp = (char*)mmap(0, screensize, PROT_READ|PROT_WRITE, MAP_SHARED, fbfd, 0);
	if (fbp == NULL) {
		printf("mmap() Failed!!");
		close(fbfd);
		return;
	}

	memcpy(fbp, (unsigned char*)output_virt, screensize);

	ercd = ioctl(fbfd, FBIOPAN_DISPLAY, &vinfo);
	if (ercd) {
		printf("ioctl(FBIOPAN_DISPLAY) Failed!! ercd=%d\n", ercd);
	}

}

static int allocate_memory(void)
{
	int ercd;
	unsigned long screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

	ercd = mmngr_alloc_in_user(
		&fd, screensize, &output_phys, &output_hard, &output_virt, MMNGR_VA_SUPPORT);
	if (ercd) {
		return ercd;
	}

	memset((void*)output_virt, 0, screensize);
	return 0;
}

static int release_memory(void)
{
	int ercd;

	ercd = mmngr_free_in_user(fd);
	return ercd;
}
