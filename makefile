#
#INC_DIR=$${SDKTARGETSYSROOT}/
#LIB_DIR=$${SDKTARGETSYSROOT}/usr/local/lib

LIBS=vspm mmngr pthread

#-I$(BUILDDIR)/include -L$(LIB_DIR)

vspm_tp: vspm_tp.c
	$${CC} -o $@ $< $(foreach LIB,$(LIBS),-l$(LIB))

clean:
	rm -f vspm_tp
